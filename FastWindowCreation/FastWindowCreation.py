import Tkinter
import tkFileDialog

def RetrieveExteriorWalls(InpFile,ExteriorWalls):
	Lines = InpFile.read().split('\n')
	ExteriorWalls = open(ExteriorWalls,'w')
	for i in range(0,len(Lines)):
		if Lines[i].find(" = EXTERIOR-WALL") > 0:
			ExteriorWalls.write(Lines[i][1:Lines[i].find(" = EXTERIOR-WALL")-1]+',0,''\n')

def CreateWindows(InpFile,OutputFile,ExteriorWalls,Windows):
		InpFile = InpFile.read().split('\n')
		ExteriorWalls = open(ExteriorWalls).read().split('\n')
		Windows = Windows.read().split('\n')
		OutputFile = open(OutputFile,'w')
		ExteriorWallBlock = 0
		WindowCounter = 1
		for i in range(0,len(InpFile)):
			if InpFile[i].find(" = EXTERIOR-WALL") > 0:
				ExteriorWallBlock = 1
				ExteriorWallName = InpFile[i][1:InpFile[i].find(" = EXTERIOR-WALL")-1]
			if InpFile[i].find('..') > 0 and ExteriorWallBlock == 1:
				ExteriorWallBlock = 0
				for j in range(0,len(ExteriorWalls)):
					ExteriorWall = ExteriorWalls[j].split(',')
					if ExteriorWallName == ExteriorWall[0]:
						WindowsCount = ExteriorWall[1]
						for k in range(2, len(ExteriorWall)-2, 3):
							WindowType = ExteriorWall[k]
							WindowX = ExteriorWall[k+1]
							WindowY = ExteriorWall[k+2]
							for l in range(0,len(Windows)):
								Window = Windows[l].split(',')
								if Window[0] == WindowType:
									WindowHeight = Window[1]
									WindowWidth = Window[2]
									WindowGlassType = Window[3]
									OutputFile.write('   ..'+'\n')
									OutputFile.write(chr(34)+'Window '+str(WindowCounter)+chr(34)+' = WINDOW'+'\n')
									OutputFile.write('   X = '+WindowX+'\n')
									OutputFile.write('   Y = '+WindowY+'\n')
									OutputFile.write('   HEIGHT = '+WindowHeight+'\n')
									OutputFile.write('   WIDTH = '+WindowWidth+'\n')
									OutputFile.write('   GLASS-TYPE = '+chr(34)+WindowGlassType+chr(34)+'\n')
									print 'Window '+str(WindowCounter)+' created as part of '+ExteriorWallName
									WindowCounter = WindowCounter + 1
			OutputFile.write(InpFile[i]+'\n')
		print "Done."
		OutputFile.close()
					
Tkinter.Tk().withdraw()

ChoiceCreateWall = raw_input("Do you want to retrieve all the exterior walls? (y/n) ")
if ChoiceCreateWall == 'y':
	InpFile = tkFileDialog.askopenfile(mode='r',title='Select a DOE-2 input file',filetypes=[('.inp','.inp'),('DOE-2 input file','.inp')])
	ExteriorWalls = raw_input("File name that will contain the exterior walls? Include extension.) ")
	RetrieveExteriorWalls(InpFile,ExteriorWalls)

ChoiceCreateWindows = raw_input("Do you want to create the windows? (y/n) ")
if 	ChoiceCreateWindows == 'y':
	InpFile = tkFileDialog.askopenfile(mode='r',title='Select a DOE-2 input file',filetypes=[('.inp','.inp'),('DOE-2 input file','.inp')])
	ExteriorWalls = raw_input("File name that will contain the exterior walls? Include extension.) ")
	Windows = tkFileDialog.askopenfile(mode='r',title='Select a CSV file that contains the window types',filetypes=[('.csv','.csv'),('CSV file','.csv')])
	OutputFile = raw_input('Output file name? Include extension. ')
	CreateWindows(InpFile,OutputFile,ExteriorWalls,Windows)

wait()