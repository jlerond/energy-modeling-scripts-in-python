Environment: DOE-2/eQUEST.
Purpose: Creates windows for a DOE-2/eQUEST model using two CSV files: 
			a) CSV file that contains the names of all the walls in the model,
			b) CSV file that contains all the window types.

Format of the CSV files:
a) Name of the Wall, Number of Windows, Window Type 1, X-coordinate, Y-coordinate, Window Type 2, etc ...
b) Window Type Name, Window Height, Window Width, Window Glass Type
