# Import PyGtk and Gtk
import pygtk
pygtk.require('2.0')
import gtk

class Main_form:

        def delete_event(self, widget, event, data=None):
            gtk.main_quit()
            return False
        
        def load_file (self, widget, label_file,comboboxentry):
            # Define the Dialog Box
            inp_file = gtk.FileChooserDialog("Select an INP file", action= gtk.FILE_CHOOSER_ACTION_OPEN, buttons = (gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL, gtk.STOCK_OPEN, gtk.RESPONSE_OK))
        
            # Define the Filter to only show INP files
            filter = gtk.FileFilter()
            filter.set_name("INP Files (*.inp)")
            filter.add_mime_type("*.inp")
            filter.add_pattern("*.inp")
            
            # Set the filter to the Dialog Box
            inp_file.add_filter(filter)
            
            # Analyse the answer from the Dialog Box
            response = inp_file.run()
            if response == gtk.RESPONSE_OK:
                label_file.set_text(inp_file.get_filename())
                inp_file.destroy()
                self.detect_keyword(label_file,comboboxentry)
            elif response == gtk.RESPONSE_CANCEL:
                print "Closed, no files selected"
                inp_file.destroy()

        def detect_keyword(self, label_file,comboboxentry):
        
            # Empty the combobox
            comboboxentry.get_model().clear()
                   
            # Open and reads the INP file
            inp_file = open(label_file.get_text(), "r")
            lines = inp_file.readlines()
            inp_file.close()

            # Initialization of the variables
            item_cb = []
            item_pres = False
            start_ok = False
            start_kw = False
            start_default = False
            end_default = False            
            eq_sign_pos = 0
            line_has_eq_sign = False
            
            # Main Loop
            for line in lines:
            
                # Test if the line corresponds to the Core of the INP file
                if "$ **      Floors / Spaces / Walls / Windows / Doors      **" in line:
                    start_ok = True
                
                # Test if the line has an equal sign and if yes if there is a space before  
                if line.find("=") != -1:
                    if line[line.index("=") - 1] == " ":
                        line_has_eq_sign = True
                        eq_sign_pos = line.index("=")
                    else:
                        line_has_eq_sign = False
                else:
                    line_has_eq_sign = False
                    eq_sign_pos = 0
                    
                # Test if the line starts only by three empty spaces
                if line[:3] == "   " and line[4] != " ":
                    start_kw = True
                else:
                    start_kw = False

                # Check if the keyword are in a SET-DEFAULT block
                if "SET-DEFAULT" in line[:11]:
                    start_default = True
                if start_default == True and "   .." in line:
                    start_default = False
                    end_default = True                    
                     
                # Check if the line is a keyword to be saved     
                if line_has_eq_sign == True and start_ok == True and start_kw == True and start_default == False and end_default == True:
                    for item in item_cb:
                        if item == line[3:eq_sign_pos -1]:
                            item_pres = True
                    if item_pres == False:
                        item_cb.append(line[3:eq_sign_pos -1])
                    else:
                        item_pres = False    
            
            # Sort the list of all the keywords retrieved
            item_cb.sort()
                                            
            # Fill the combobox                                             
            for item in item_cb:
                comboboxentry.append_text(item.strip())
        
        def default_keyword (self, widget, label_file, comboboxentry):
            
            # Initialization of the variables
            last_lines_kw = False
            lines_following_kw = False
            item_cb = []
            pres_cb = False
            
            # Get the items in the combobox
            model = comboboxentry.get_model()
            for i, k in enumerate(model):
               itr = model.get_iter(i)
               title = model.get_value(itr,0)  
               item_cb.append(title)          
            
            # Check if the selected item is in the combobox, ie a valid keyword
            for item in item_cb:
                if item == comboboxentry.get_active_text():
                    pres_cb = True
            
            # Check if a INP file has been loaded
            if label_file.get_text() != "" and comboboxentry.get_active_text() != "" and pres_cb == True:
            
                # Open and read the lines of the INP file
                inp_file = open(label_file.get_text(),"r")
                lines = inp_file.readlines()
                inp_file.close()
                
                # Re-open the INP file to re-write it with the default
                inp_file = open(label_file.get_text(), "w")
                
                # Intilialization of the loop variables
                start_kw = False
                start_default = False
                end_default = False
                eq_sign_pos = 0
                line_has_eq_sign = False
                lines_following_kw = False
                keyword = False   
                            
                # Loop through the line to identify the ones to re-write in the file and the ones to not
                for line in lines:
                
                    # Test if the line has an equal sign and if yes if there is a space before  
                    if line.find("=") != -1:
                        if line[line.index("=") - 1] == " ":
                            line_has_eq_sign = True
                            eq_sign_pos = line.index("=")
                        else:
                            line_has_eq_sign = False
                    else:
                        line_has_eq_sign = False
                        eq_sign_pos = 0
                        
                    # Test if the line starts only by three empty spaces
                    if line[:3] == "   " and line[4] != " ":
                        start_kw = True
                    else:
                        start_kw = False               
                
                    # Check if the line starts and end like a keyword
                    if line_has_eq_sign == True and start_kw == True:
                        keyword = True
                        lines_following_kw = False
                    else:
                        keyword = False
                    
                    # Check if the line is in a SET-DEFAULT block
                    if "SET-DEFAULT" in line[:11]:
                        start_default = True
                    if start_default == True and "   .." in line:
                        start_default = False
                        end_default = True
                
                    # Check if the line is a keyword to be saved
                    if start_default == False and end_default == True:
                        if comboboxentry.get_active_text() in line or (lines_following_kw == True and keyword == False):
                            lines_following_kw = True
                            keyword = False
                            last_lines_kw = True
                        else:
                            inp_file.write(line)
                            keyword = False
                            last_lines_kw = False
                    else:
                            inp_file.write(line)

                # Close the INP file                            
                inp_file.close()
                
                # Re-populates the combobox
                self.detect_keyword(label_file,comboboxentry)   
                
                # Notifies the user of the end of the procedure
                md = gtk.MessageDialog(None, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_INFO,gtk.BUTTONS_OK, "The keyword has been defaulted.")
                md.run()
                md.destroy()
            
            else: 
            
                # Notifies the user that something wnet wrong
                md = gtk.MessageDialog(None, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_INFO,gtk.BUTTONS_OK, "Please, choose a valid INP file or/and a valid keyword.")
                md.run()
                md.destroy()   
            
        def __init__(self):
            
            # Creates the window
            self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
            
            # Set the window's icon
            self.window.set_icon_from_file("icon.png")
            
            # Define the window as un-resizable
            self.window.set_resizable(False)
            
            # Set the window's title
            self.window.set_title("eQUEST Defaulting")
            
            # Connect the window to the delete_event signal
            self.window.connect("delete_event", self.delete_event)
            
            # Set the border of the window
            self.window.set_border_width(10)
            
            # Define the labels
            label_title = gtk.Label("INP File:")
            label_file = gtk.Label("-")
            label_keyword = gtk.Label("Keyword to be defaulted:")   
            
            # Define Entries
            entry_keyword = gtk.Entry(max=0) 
            
            # Define the Combobox
            comboboxentry = gtk.combo_box_entry_new_text()              
            
            # Define the buttons
            button_load_file = gtk.Button("Load INP file")
            button_default_kw = gtk.Button("Default")            
            button_load_file.connect("clicked", self.load_file, label_file,comboboxentry)
            button_default_kw.connect("clicked", self.default_keyword, label_file, comboboxentry)   
            
            # Define the boxes
            hbox = gtk.HBox(False, 5)
            hbox_1st = gtk.HBox(False,5)
            hbox_2nd = gtk.HBox(False,5)
            vbox_1st = gtk.VBox(False, 5)
            vbox_2nd = gtk.VBox(False,5)
            
            hbox_1st.pack_start(vbox_1st, False, False, 10) 
            vbox_1st.pack_start(label_title, False, False, 5)
            vbox_1st.pack_start(label_file, False, False, 0)
            vbox_1st.pack_start(comboboxentry, False, False, 0)
            
            hbox_2nd.pack_start(vbox_2nd, False, False, 10)
            vbox_2nd.pack_start(button_load_file, False, False, 5)
            vbox_2nd.pack_start(button_default_kw, False, False, 7)
            
            hbox.pack_start(hbox_1st, False, False, 0)  
            hbox.pack_start(hbox_2nd, False, False, 0)              
            
            self.window.add(hbox)               
            
            self.window.show_all()
              
def main():
    gtk.main()
    return 0
    
if __name__ == "__main__":
    Main_form()
    main()