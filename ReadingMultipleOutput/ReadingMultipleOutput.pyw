# Usage: aggregate results from parametric runs in a CSV file
# last update: 2/11/2015
# Author: Madison Engineering PS (Jeremy L.)

# import module
import easygui
from eppy import readhtml

# define list of HTML files to go through
filenamelist =[
r'Z:\Modeling Runs\Aegis West Seattle\6-25-2015\AegisWestSeattleJune2015PSZ-BaselineWSECTable.html',
r'Z:\Modeling Runs\Aegis West Seattle\6-25-2015\AegisWestSeattleJune2015PSZ-BaselineSECTable.html',
r'Z:\Modeling Runs\Aegis West Seattle\6-25-2015\AegisWestSeattleJune2015PSZ-CDBldgTable.html',
r'Z:\Modeling Runs\Aegis West Seattle\6-25-2015\AegisWestSeattleJune2015PSZ-LtgRd10Table.html',
r'Z:\Modeling Runs\Aegis West Seattle\6-25-2015\AegisWestSeattleJune2015PSZ-LtgRd20Table.html',
r'Z:\Modeling Runs\Aegis West Seattle\6-25-2015\AegisWestSeattleJune2015PSZ-LwFlwFixtTable.html',
r'Z:\Modeling Runs\Aegis West Seattle\6-25-2015\AegisWestSeattleJune2015PSZ-CondWtrHtrTable.html',
r'Z:\Modeling Runs\Aegis West Seattle\6-25-2015\AegisWestSeattleJune2015PSZ-HvacUpgradeTable.html',
r'Z:\Modeling Runs\Aegis West Seattle\6-25-2015\AegisWestSeattleJune2015PSZ-BundleTable.html',
r'Z:\Modeling Runs\Aegis West Seattle\6-25-2015\AegisWestSeattleJune2015VRF-CDBldgTable.html',
r'Z:\Modeling Runs\Aegis West Seattle\6-25-2015\AegisWestSeattleJune2015VRF-BundleTable.html'
]

# define output file
fileoutput = open('C:\Users\KJM1\Documents\Building Energy Modeling\~Ressources~\Energy Plus\Output\output.csv', 'w')
result_elec = ""
result_gas = ""
index_counter = 1

def header(fileoutput,htables,k,j):
    fileheader = ""
    for j in range(1, len(htables[k][1])):
        fileheader = fileheader + str(htables[k][1][j][0]) + ","  
    fileoutput.write(",," + fileheader + "," + fileheader + "\n")
    return True

# pull out all the end-use in a csv for all the HTML files defined previously
for i in range(0,len(filenamelist)):
    filehandle = open(filenamelist[i], 'r').read()
    htables = readhtml.titletable(filehandle)
    
    #fileoutput.write(filenamelist[i]+"\n")
    
    for k in range(0,len(htables)):
        if htables[k][0] == 'EAp2-4/5. Performance Rating Method Compliance':
            l = k
            for j in range(1, len(htables[k][1])):
                result_elec =  result_elec + str(htables[k][1][j][1]) + ","
                result_gas =  result_gas + str(htables[k][1][j][3]) + ","
    #print header_already_present          
    if i == 0:
         header(fileoutput,htables,l,j)
         fileoutput.write(str(index_counter) + "," + filenamelist[i] + "," + result_elec + "," + result_gas+ "\n")
         result_elec = "" 
         result_gas = "" 
    else:
        fileoutput.write(str(index_counter) + "," + filenamelist[i] + "," + result_elec + "," + result_gas+ "\n")              
        result_elec = "" 
        result_gas = ""      
    index_counter = index_counter + 1  
fileoutput.close()
easygui.msgbox("Done.", title="Reading E+ Html Tables")