Environment: EnergyPlus.
Purpose: Retrieve annual energy consumption of each end use.
Notes: The list 'filenamelist' needs to be updated and contain the path to each EnergyPlus HTML table that contains simulation results.
Requirements: Eppy is needed to execute the script.