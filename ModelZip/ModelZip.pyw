
# coding: utf-8

# In[1]:

import zipfile
import os


# In[2]:

def zipmodel(path,name):
    zf = zipfile.ZipFile("{path}{file}.zip".format(path=path, file=name),"w")
    
    # Includes inp file if it exists
    if os.path.exists(path+name+".inp"):
        zf.write(path+name+".inp",os.path.basename(name)+".inp")
    
    # Includes pd2 if it exists
    if os.path.exists(path+name+".pd2"):
        zf.write(path+name+".pd2",os.path.basename(name)+".pd2")    
    
    # Includes prd if it exists
    if os.path.exists(path+name+".prd"):
        zf.write(path+name+".prd",os.path.basename(name)+".prd")
    
    zf.close()


# In[3]:

model_path = raw_input("Path? (with quotes) ")
model_name = raw_input("Name? ")
model_path = model_path[1:-1] + chr(92)

# In[4]:

zipmodel(model_path,model_name)


# In[ ]:



