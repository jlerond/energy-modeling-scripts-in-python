import subprocess
import easygui
from tempfile import mkstemp
from shutil import move, copyfile
from os import remove, close, getcwd

# Run EnergyPlus
def RunEnergyPlus(idf, weather):
	command = 'EnergyPlus.exe -x -w {0} -p {1} {2}'.format(weather,idf[:-len('.idf')], idf)
	print command
	EnergyPlus = subprocess.call(command)
	if CheckConvergence(idf) == 1:
		print "The simulation converged!"
	else:
		print "The simulation did not converge. Increasing the time step and simulating again..."
		FixTimeStep(idf,weather)

# Check if the EnergyPlus run converged
def CheckConvergence(idf):
	convergence = 1
	check = -1
	with open(idf[:-4]+'out.err') as f:
		for line in f:
			check = line.find('** Severe  ** CheckWarmupConvergence:')
			if check <> -1:
				convergence = 0
	return convergence

def FixTimeStep(idf,weather):
	increment = 5
	timestepnextline = 0
	fh, abs_path = mkstemp()
	with open(idf,'r') as f:
		with open(abs_path,'w') as nf:
			for line in f:
				timestepline = line.find('Timestep,')
				if timestepnextline == 1:
					sep = line.find(";")
					timestep = line[:sep].strip()
					timestepnextline = 0
					newtimestep = int(timestep) + int(increment)
					nf.write(str(newtimestep)+';\n')
				else:
					nf.write(line)
				if timestepline <> -1:
					timestepnextline = 1
	close(fh)
	f.close()
	remove(idf)
	move(abs_path, idf)
	RunEnergyPlus(idf,weather)
	
# Initialization
EnergyPlusRuns = "EnergyPlusRuns.csv"
PathToIDD = 'C:\\EnergyPlusV8-3-0\\Energy+.idd'
NewPathToIDD = getcwd() + "\\Energy+.idd"
copyfile(PathToIDD,NewPathToIDD)

# Retrieve the run
with open(EnergyPlusRuns,'r') as f:
	for line in f:
		sep = line.find(',')
		RunEnergyPlus(line[:sep],line[sep+1:-1])
easygui.msgbox("All the runs converged.", title="Run Energy Plus w/ Incremental Time Step")